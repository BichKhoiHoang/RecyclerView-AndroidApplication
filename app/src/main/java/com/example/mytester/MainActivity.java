package com.example.mytester;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<FeedElement> elements = new ArrayList<>();
    int[] foodImages = { R.drawable.baseline_photo_24,
            R.drawable.baseline_photo_24,
            R.drawable.baseline_photo_24,
            R.drawable.baseline_photo_24,
            R.drawable.baseline_photo_24};
    int[] avatarImages = {R.drawable.baseline_account_circle_24,
            R.drawable.baseline_account_circle_24,
            R.drawable.baseline_account_circle_24,
            R.drawable.baseline_account_circle_24,
            R.drawable.baseline_account_circle_24};

    int[] locationSymbols = {R.drawable.baseline_location_on_24,
            R.drawable.baseline_location_on_24,
            R.drawable.baseline_location_on_24,
            R.drawable.baseline_location_on_24,
            R.drawable.baseline_location_on_24};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        setUpElements();

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, elements);

        recyclerView.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUpElements() {
        String[] titles = {"Give away carrots!!",
                "Give away tomatoes...",
                "Give away potatoes!",
                "Give away veggies!!",
                "Give away eggs"};
        String[] usernames = {"ant12", "monkey11", "bird00", "rose@3", "flyXx1"};
        int[] postCodes = {4000, 4304, 4059, 4001, 4059};
        String[] expiredDates = {"19/12/2030", "19/12/2030", "19/12/2030", "19/12/2030", "19/12/2030"};

        for (int i = 0; i < 5; i++){
            elements.add(new FeedElement(titles[i],
                    usernames[i], postCodes[i],expiredDates[i],
                    foodImages[i], avatarImages[i], locationSymbols[i]));
        }
    }
}