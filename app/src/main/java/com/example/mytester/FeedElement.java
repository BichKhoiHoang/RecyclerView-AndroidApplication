package com.example.mytester;

public class FeedElement {
    String title;
    String username;
    int postcode;
    String expiredDate;
    int foodImage;
    int avatarImage;
    int locationSymbol;

    public FeedElement(String title, String username, int postcode, String expiredDate, int foodImage, int avatarImage, int locationSymbol) {
        this.title = title;
        this.username = username;
        this.postcode = postcode;
        this.expiredDate = expiredDate;
        this.foodImage = foodImage;
        this.avatarImage = avatarImage;
        this.locationSymbol = locationSymbol;
    }


    public String getTitle() {
        return title;
    }

    public int getPostcode() {
        return postcode;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public int getFoodImage() {
        return foodImage;
    }

    public int getAvatarImage() {
        return avatarImage;
    }

    public String getUsername() {
        return username;
    }

    public int getLocationSymbol(){ return locationSymbol;}
}
