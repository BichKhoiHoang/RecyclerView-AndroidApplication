package com.example.mytester;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    Context context;
    ArrayList<FeedElement> elements;

    public RecyclerViewAdapter(Context context, ArrayList<FeedElement> elements){
        this.context = context;
        this.elements = elements;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.post_row, parent, false);

        return new RecyclerViewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // assigning values to the views we created in the recycler_view_row layout file
        // based on the position of the recycler view
        holder.title.setText(elements.get(position).getTitle());
        holder.username.setText(elements.get(position).getUsername());
        holder.postcode.setText(elements.get(position).getPostcode());
        holder.expiredDate.setText(elements.get(position).getExpiredDate());
        holder.foodImage.setImageResource(elements.get(position).getFoodImage());
        holder.avatarImage.setImageResource(elements.get(position).getAvatarImage());
        holder.locationSymbol.setImageResource(elements.get(position).getLocationSymbol());
    }

    @Override
    public int getItemCount() {
        // the recycler view just wants to know the num of items you want displayed
        return elements.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // grab the views from our recycler_view_row layout file
        ImageView foodImage, avatarImage, locationSymbol;
        TextView title, username, postcode, expiredDate;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            foodImage = itemView.findViewById(R.id.food_photo);
            avatarImage = itemView.findViewById(R.id.avatar);
            title = itemView.findViewById(R.id.post_title);
            username = itemView.findViewById(R.id.user_name);
            postcode = itemView.findViewById(R.id.post_code);
            expiredDate = itemView.findViewById(R.id.expired_date);
            locationSymbol = itemView.findViewById(R.id.location);
        }
    }
}
